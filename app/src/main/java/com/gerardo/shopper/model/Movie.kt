package com.gerardo.shopper.model

import android.graphics.Bitmap
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Gerardo on 11/04/2020.
 * Clase base para modelar los datos que seran almacenados en la base de datos dentro del movil
 */
@Entity(tableName = "Movies")
data class Movie (
    @PrimaryKey(autoGenerate = false) var id : Int,
    @ColumnInfo(name = "title") var title: String,
    @ColumnInfo(name = "average") var vote_average: Double,
    @ColumnInfo(name = "overview") var overview: String,
    @ColumnInfo(name = "poster") var poster_path: String,
    @ColumnInfo(name = "page") var page: Int,
    @ColumnInfo(name = "local") var local: Boolean,
    @ColumnInfo(name = "stringIMG") var stringIMG: String
)