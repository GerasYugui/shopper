package com.gerardo.shopper.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.gerardo.shopper.R
import com.gerardo.shopper.model.MovieResponse
import com.gerardo.shopper.utils.AppConstants
import com.gerardo.shopper.utils.AppConstants.MOVIE_DATA

class DetailActivity : AppCompatActivity() {

    private lateinit var movieResponse: MovieResponse

    private lateinit var movieImg : ImageView
    private lateinit var movieTitle : TextView
    private lateinit var movieOverview : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        // obtenemos los datos enviados en el fragment
        movieResponse = intent.extras?.get(MOVIE_DATA) as MovieResponse

        initviews()

    }

    private fun initviews(){
        movieImg = findViewById(R.id.detail_movie_img)
        movieTitle = findViewById(R.id.movie_title)
        movieOverview = findViewById(R.id.movie_overview)
        Glide.with(this).load(AppConstants.POSTER_URL + movieResponse.poster_path).into(movieImg)
        movieTitle.text = movieResponse.title
        movieOverview.text = movieResponse.overview
        supportActionBar?.setTitle(movieResponse.title)
    }
}
