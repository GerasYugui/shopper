package com.gerardo.shopper.ui.fragment

import android.Manifest
import android.app.ActivityOptions
import android.app.DownloadManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gerardo.shopper.R
import com.gerardo.shopper.model.Movie
import com.gerardo.shopper.model.MovieResponse
import com.gerardo.shopper.source.local.AppDatabase
import com.gerardo.shopper.ui.DetailActivity
import com.gerardo.shopper.ui.adapter.MovieAdapter
import com.gerardo.shopper.ui.custom.InfoDialogFragment
import com.gerardo.shopper.ui.viewmodel.MainViewModel
import com.gerardo.shopper.utils.AppConstants.MOVIE_DATA
import com.gerardo.shopper.utils.AppConstants.POSTER_URL
import com.gerardo.shopper.utils.encodeImage
import com.gerardo.shopper.utils.isConnected
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream
import java.io.File


class MainFragment : Fragment(), MovieAdapter.OnItemClickListener, MovieAdapter.OnTitleItemClickListener{

    lateinit var recyclerViewAction : RecyclerView
    lateinit var recyclerViewAnimation: RecyclerView
    lateinit var recyclerViewComedy  : RecyclerView
    lateinit var searchMovie  : SearchView
    var movieAdapter: MovieAdapter = MovieAdapter()
    var movieAdapter2: MovieAdapter = MovieAdapter()
    var movieAdapter3: MovieAdapter = MovieAdapter()

    var movieIMGPath = ""

    var msg: String? = ""
    var lastMsg = ""

    private lateinit var root: View

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        root = inflater.inflate(R.layout.main_fragment, container, false)

        initViews()
        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        activity?.let {
            viewModel = ViewModelProviders.of(it).get(MainViewModel::class.java)
        }
        observeInput()
    }

    /**
     * Inicializamos los componentes de la vista
     */
    private fun initViews(){
        recyclerViewAction = root.findViewById(R.id.recycler_action_section)
        recyclerViewAnimation = root.findViewById(R.id.recycler_animation_section)
        recyclerViewComedy = root.findViewById(R.id.recycler_comedy_section)

        searchMovie = root.findViewById(R.id.movie_search)

        // custom search view

        val searchIcon = searchMovie.findViewById<ImageView>(R.id.search_mag_icon)
        searchIcon.setColorFilter(Color.WHITE)
        val cancelIcon = searchMovie.findViewById<ImageView>(R.id.search_close_btn)
        cancelIcon.setColorFilter(Color.WHITE)
        val textView = searchMovie.findViewById<TextView>(R.id.search_src_text)
        textView.setTextColor(Color.WHITE)

        searchMovie.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                movieAdapter.filter.filter(newText)
                movieAdapter2.filter.filter(newText)
                movieAdapter3.filter.filter(newText)
                return false
            }

        })
    }

    /**
     * Se inicializan los observadores que se activaran cuando los datos que recibe la vista se actualizan.
     * Adémas se define el tratamiento de los datos recibidos.
     */
    private fun observeInput(){
        viewModel.popular.observe(viewLifecycleOwner, Observer {
            it?.let {
                context?.let { it1 -> movieAdapter.MovieAdapter(it1,it, this, this, 1) }

                recyclerViewAction.adapter = movieAdapter

                recyclerViewAction.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            }
        })

        viewModel.yourSelection.observe(viewLifecycleOwner, Observer {
            it?.let {
                context?.let { it1 -> movieAdapter2.MovieAdapter(it1,it, this, this, 2) }
                recyclerViewAnimation.adapter = movieAdapter2
                recyclerViewAnimation.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            }
        })
        viewModel.trends.observe(viewLifecycleOwner, Observer {
            it?.let {
                context?.let { it1 -> movieAdapter3.MovieAdapter(it1,it, this, this, 3) }
                recyclerViewComedy.adapter = movieAdapter3
                recyclerViewComedy.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            }
        })
    }

    /**
     * Metodo para abrir el detalle de la pelicula, se envia la información al siguiente fragmento.
     * @param movie
     * @param movieImageView
     */
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onItemClicked(movie: MovieResponse, movieImageView: ImageView, page: Int) {
        // guardamos localmente la pelicula consultada para su futura revixión sin internet
        if (context?.let {ContextCompat.checkSelfPermission(it,Manifest.permission.WRITE_EXTERNAL_STORAGE)} == PackageManager.PERMISSION_GRANTED){
            if (context?.isConnected()!!){
                Log.d("Movies", "Entro a itemcliked permiso ganado")
                downloadImage(movie, page)
            }
        }
        var intent = Intent(activity, DetailActivity::class.java)
        intent.putExtra(MOVIE_DATA, movie)

        // creamos la animación
        val activityOptions : ActivityOptions = ActivityOptions.makeSceneTransitionAnimation(activity, movieImageView, "sharedName")
        startActivity(intent, activityOptions.toBundle())

    }

    /**
     * Metodo para abrir el dialog fragment
     * @param movie
     */
    override fun onTitleItemClicked(movie: MovieResponse, page: Int) {
        // guardamos localmente la pelicula consultada para su futura revixión sin internet
        if (context?.let {ContextCompat.checkSelfPermission(it,Manifest.permission.WRITE_EXTERNAL_STORAGE)} == PackageManager.PERMISSION_GRANTED){
            if (context?.isConnected()!!){
                downloadImage(movie, page)
            }
        }
        // Open dialog fragment
        val ft = fragmentManager?.beginTransaction()
        val newFragment = InfoDialogFragment.newInstance(movie)
        if (ft != null) {
            newFragment.show(ft, "dialog")
        }
//        context?.showToast(movie.title)
    }

    /**
     * Guardamos la pelicula en la base local para su posterior acceso sin internet
     */
    private fun saveMovieDB(movie: MovieResponse, page: Int, encoded: String){
        GlobalScope.launch {
            val db = context?.let { AppDatabase.Companion.invoke(it) }
            val movieDB = db?.movieDao()?.findById(movie.id)
            if (movieDB == null){
                Log.d("Movies", "Entro a insertar")
                db?.movieDao()?.insertAll(Movie(movie.id, movie.title, movie.vote_average, movie.overview, movie.poster_path, page, true, encoded))
            }

        }
    }



    /**
     * Metodo para descarga de imagenes, necesario para la consulta de peliculas fuera de linea
     */
    private fun downloadImage(movie: MovieResponse, page: Int) {
        Picasso.get().load(POSTER_URL + movie.poster_path).into(object : Target{
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {

            }

            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {

            }

            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                val byteArrayOutputStream = ByteArrayOutputStream()
                bitmap?.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
                val byteArray: ByteArray = byteArrayOutputStream.toByteArray()
                val encoded: String = Base64.encodeToString(byteArray, Base64.DEFAULT)
                saveMovieDB(movie, page, encoded)
            }

        })
    }

}