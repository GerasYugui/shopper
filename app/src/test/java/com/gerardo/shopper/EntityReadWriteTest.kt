package com.gerardo.shopper

import androidx.room.Room
import androidx.test.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.gerardo.shopper.model.Movie
import com.gerardo.shopper.source.local.AppDatabase
import com.gerardo.shopper.source.local.MovieDao
import org.hamcrest.CoreMatchers.equalTo
import org.junit.After
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class EntityReadWriteTest {
    private lateinit var movieDao: MovieDao
    private lateinit var db: AppDatabase

    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getContext()
        db = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java).build()
        movieDao = db.movieDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun writeUserAndReadInList() {
        val todo: Movie = Movie(1, "Prueba", 1.0, "Overview prueba", "PATH")
        movieDao.insertAll(todo)
        val todoItem = movieDao.findByTitle(todo.title)
        assertThat(todoItem, equalTo(todo))
    }
}