package com.gerardo.shopper.source.remote

import com.gerardo.shopper.model.TheMovieDBResponse
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Creado por Gerardo Tinajero 7/04/2020
 */
interface APIService{

    /**
     * Metodo para obtener un listado de peliculas
     */
    @GET("3/discover/movie")
    fun getMovieList(@Query("api_key") apiKey : String, @Query("language") language : String,
                     @Query("sort_by") sortBy : String, @Query("include_adult") includeAdult : Boolean,
                     @Query("include_video") includeVideo : Boolean, @Query("page") page : Int): Observable<TheMovieDBResponse>

    /*@POST("Users/StoreLogin")
    fun login(@Body loginRequest: LoginRequest): Call<LoginResponse>*/
}