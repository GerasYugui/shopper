package com.gerardo.shopper.model

data class TheMovieDBResponse (
    var page: Int,
    var total_results: Int,
    var total_pages: Int,
    var results : MutableList<MovieResponse>
)