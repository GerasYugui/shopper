package com.gerardo.shopper.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gerardo.shopper.model.MovieResponse

class MainViewModel : ViewModel() {
    val popular = MutableLiveData<MutableList<MovieResponse>>()
    val yourSelection = MutableLiveData<MutableList<MovieResponse>>()
    val trends = MutableLiveData<MutableList<MovieResponse>>()
}