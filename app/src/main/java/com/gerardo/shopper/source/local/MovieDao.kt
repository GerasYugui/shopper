package com.gerardo.shopper.source.local

import androidx.room.*
import com.gerardo.shopper.model.Movie

/**
 * Created by Gerardo on 11/04/2020.
 * Interface que provee los metodos para acceder y administrar los  datos
 */
@Dao
interface MovieDao {
    @Query("SELECT * FROM Movies")
    fun getAll(): List<Movie>

    @Query("SELECT * FROM Movies WHERE id = :id")
    fun findById(id: Int): Movie

    @Insert
    fun insertAll(vararg todo: Movie)

    @Delete
    fun delete(todo: Movie)

    @Update
    fun updateTodo(vararg todos: Movie)

    @Query("SELECT * FROM Movies WHERE page = :page")
    fun getSection(page: Int): MutableList<Movie>

}