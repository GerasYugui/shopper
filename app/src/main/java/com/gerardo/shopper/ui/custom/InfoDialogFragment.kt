package com.gerardo.shopper.ui.custom

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.DialogFragment
import com.bumptech.glide.Glide
import com.gerardo.shopper.R
import com.gerardo.shopper.model.MovieResponse
import com.gerardo.shopper.utils.AppConstants

class InfoDialogFragment : DialogFragment() {

    private lateinit var movie: MovieResponse

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //Recibimos los datos de la pelicula
        movie = arguments?.getSerializable(AppConstants.MOVIE_DATA) as MovieResponse

        // Pick a style based on the num.
        val style = DialogFragment.STYLE_NO_FRAME
        val theme = R.style.DialogTheme
        setStyle(style, theme)
    }

    // Override the Fragment.onAttach() method to instantiate the
    // NoticeDialogListener
    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater!!.inflate(R.layout.layout_dialog, container, false)

        initView(view)

        return view
    }

    /**
     * Se inicializan los componentes de la vista
     */
    private fun initView(view: View){
        val movieImg = view.findViewById<View>(R.id.detail_movie_img) as ImageView
        val movieTitle = view.findViewById<View>(R.id.title) as TextView
        val movieRating = view.findViewById<View>(R.id.ratingBar) as RatingBar
        val closeButton = view.findViewById<View>(R.id.close_button) as Button

        movieTitle.text = movie.title
        context?.let { Glide.with(it).load(AppConstants.POSTER_URL + movie.poster_path).into(movieImg) }
        movieRating.rating = movie.vote_average.toFloat() / 2

        closeButton.setOnClickListener {
            dismiss()
        }
    }

    companion object {
        /**
         * Metodo para crear una nueva instancia del dialogfragment y recibir los parametros
         */
        fun newInstance(movie: MovieResponse): InfoDialogFragment {
            val f = InfoDialogFragment()

            // Supply num input as an argument.
            val args = Bundle()
            args.putSerializable(AppConstants.MOVIE_DATA, movie)
            f.arguments = args

            return f
        }
    }
}