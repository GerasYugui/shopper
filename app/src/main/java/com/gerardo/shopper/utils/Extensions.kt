package com.gerardo.shopper.utils

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.ConnectivityManager
import android.util.Base64
import android.view.View
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import java.io.ByteArrayOutputStream
import java.io.File

fun Context.showToast(message: String, duration: Int = Toast.LENGTH_SHORT){
    Toast.makeText(this, message, duration).show()
}

fun showSnackbar(view: View, message: String, duration: Int = Snackbar.LENGTH_LONG){
    Snackbar.make(view, message, duration).setAction("Action", null).show()
}

fun Context.basicAlert(view: View, title: String, message: String){
    val builder : AlertDialog.Builder = AlertDialog.Builder(this)
    builder.setTitle(title)
    builder.setMessage(message)
    builder.setPositiveButton("Ok", DialogInterface.OnClickListener(function = dismissDialog))
    builder.show()
}

val dismissDialog = { dialog: DialogInterface, which: Int ->
    dialog.dismiss()
}

fun Context.isConnected(): Boolean{
    var connected = false
    val connec : ConnectivityManager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val networkInfo = connec.activeNetworkInfo
    connected = networkInfo != null && networkInfo.isConnected

    return connected
}

fun encodeImage(image: File): String{
    val bm : Bitmap = BitmapFactory.decodeFile(image.absolutePath)
    val baos = ByteArrayOutputStream()
    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos)
    val b : ByteArray = baos.toByteArray()
    return Base64.encodeToString(b, Base64.DEFAULT)
}

fun Context.decodeImage(imageString: String): Bitmap {
    val imageBytes : ByteArray = Base64.decode(imageString, Base64.DEFAULT)
    return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
}