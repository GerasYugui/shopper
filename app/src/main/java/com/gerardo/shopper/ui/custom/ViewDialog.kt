package com.gerardo.shopper.ui.custom

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Window
import android.widget.ProgressBar
import com.gerardo.shopper.R

/**
 * Created by Gerardo on 7/04/2020.
 */

class ViewDialog {

    private lateinit var activity: Activity
    private lateinit var dialog: Dialog


    constructor(activity: Activity){
        this.activity = activity
    }

    fun showDialog(){
        dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.setCancelable(false)
        dialog.setContentView(R.layout.loading_layout)

        var  gifImageView: ProgressBar = dialog.findViewById(R.id.custom_loading_imageView)

        dialog.show()

//        Flubber.with()
//            .animation(Flubber.AnimationPreset.SQUEEZE)
//            .repeatCount(100)
//            .duration(1000)
//            .createFor(gifImageView)
//            .start()
    }

    fun hideDialog(){
        dialog.dismiss()
    }

}