package com.gerardo.shopper.source.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.gerardo.shopper.model.Movie

/**
 * Created by Gerardo on 11/04/2020.
 * Escribir la base de datos que contiene todos sus DAO como métodos abstractos.
 */
@Database(entities = [Movie::class], version = 2)
abstract class AppDatabase : RoomDatabase(){
    abstract fun movieDao() :  MovieDao
    companion object {
        @Volatile private var instance: AppDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context)= instance ?: synchronized(LOCK){
            instance ?: buildDatabase(context).also { instance = it}
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(context,
            AppDatabase::class.java, "movies.db")
            .fallbackToDestructiveMigration()
            .build()
    }

}