package com.gerardo.shopper.source.remote

import android.app.Service
import android.content.Context
import android.content.IntentFilter
import android.util.Log
import androidx.fragment.app.Fragment
import com.gerardo.shopper.R
import com.gerardo.shopper.model.ShopperError
import com.gerardo.shopper.model.TheMovieDBResponse
import com.gerardo.shopper.utils.AppConstants
import com.gerardo.shopper.utils.AppConstants.GET_MOVIE_LIST
import com.gerardo.shopper.utils.AppConstants.GET_MOVIE_LIST_2
import com.gerardo.shopper.utils.AppConstants.GET_MOVIE_LIST_3
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Creado por Gerardo Tinajero 7/04/2020
 * Clase con todos los metodos necesarios para realizar los request al servidor y regresar la respuesta a la clase que invoco el metodo.
 */
class NetHelper() {

    private val DEVELOPMENT_PATH: String = "https://api.themoviedb.org/"
    private val RELEASE_PATH: String = ""
    private val LOCALHOST_PATH: String = ""

    /**
     * Inicializamos retrofit para el consumo de los WS, aqui se define la URl del servidor al cual deseamos apuntar (Deveop, Producción o localhost)
     */
    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getPathMode())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    /**
     * Funcion que nos permite interceptar la petición con retrofit para añadir parametros a nuestro request, como es el token
     */
    private fun getOkHttpClient(token: String): OkHttpClient {
        return OkHttpClient.Builder().addInterceptor { chain ->
            var newRequet: Request =
                chain.request().newBuilder().addHeader("Authorization", "Bearer " + token).build()
            chain.proceed(newRequet)
        }.build()
    }

    /**
     * Sobrecarga de la funcion principal que permite añadir el interceptor de nuesto request para añadir el token
     */
    private fun getRetrofit(token: String): Retrofit {
        return Retrofit.Builder()
            .client(getOkHttpClient(token))
            .baseUrl(getPathMode())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    /**
     * Ineterface que nos permite manejar de forma sencilla las respuestas de los WS solicitados desde otras clases.
     */
    interface OnDataResultInterface {
        fun OnResultOk(any: Any, code: Int)
        fun OnError(error: ShopperError, code: Int)
        fun OnCollectionResult(results: MutableList<Any>, code: Int)
    }

    private lateinit var onDataResultInterface: OnDataResultInterface

    constructor(context: Context) : this() {
        onDataResultInterface = context as OnDataResultInterface
    }

    constructor(context: Fragment) : this() {
        onDataResultInterface = context as OnDataResultInterface
    }

    constructor(context: Service) : this() {
        onDataResultInterface = context as OnDataResultInterface
    }

    fun getPathMode(): String {
        if (AppConstants.DEBUG_LOCAL) {
            return LOCALHOST_PATH
        } else if (AppConstants.DEBUG) {
            return DEVELOPMENT_PATH
        } else {
            return RELEASE_PATH
        }
    }

    /**
     * Realiza el request de las peliculas
     */
    fun getMovies(
        apiKey: String,
        language: String,
        sortBy: String,
        includeAdult: Boolean,
        includeVideo: Boolean,
        page: Int
    ) {
        var myCompositeDisposable: CompositeDisposable = CompositeDisposable()
        val requestBody = getRetrofit().create(APIService::class.java)

        myCompositeDisposable.add(requestBody.getMovieList(apiKey, language, sortBy, includeAdult, includeVideo, page).observeOn(
            AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(this::handleGetMoviesResponse,this::handleGetMoviesError))

    }

    /**
     * Espera una respuesta y con base en esta, pasa el dato a la clase que la invoco.
     */
    private fun handleGetMoviesResponse(movies: TheMovieDBResponse){
        if (movies.results.size > 0 ){
            if (movies.page == 1)
                onDataResultInterface.OnCollectionResult(movies.results as MutableList<Any>, GET_MOVIE_LIST)
            if (movies.page == 2)
                onDataResultInterface.OnCollectionResult(movies.results as MutableList<Any>, GET_MOVIE_LIST_2)
            if (movies.page == 3)
                onDataResultInterface.OnCollectionResult(movies.results as MutableList<Any>, GET_MOVIE_LIST_3)
        }else{
            val error = ShopperError(R.string.error_movie_list_request)
            onDataResultInterface.OnError(error, GET_MOVIE_LIST)
        }
    }

    /**
     * En caso de que el request de obtener peliculas tenga un error, aqui se maneja el error a la clase que invoco el metodo.
     */
    private fun handleGetMoviesError(error: Throwable){
        Log.d("Token", error.message)
        val error = ShopperError(R.string.error_movie_list_request)
        onDataResultInterface.OnError(error, GET_MOVIE_LIST)
    }

}