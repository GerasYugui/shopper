package com.gerardo.shopper.ui

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.gerardo.shopper.R
import com.gerardo.shopper.model.MovieResponse
import com.gerardo.shopper.utils.AppConstants
import com.gerardo.shopper.utils.isConnected

/**
 * Created by Gerardo on 7/04/2020.
 */
class SplashScreen : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
    }

    /**
     * En este metodo del ciclo de vida de la aplicaciòn se debe cargar informaciòn necesaria como alguna configuraciòn
     * antes de proceder al Home, pero como no es el caso, solo esperamos el tiempo y abrimos home.
     */
    public override fun onStart() {
        super.onStart()
        Handler().postDelayed({
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Build.VERSION.SDK_INT <= Build.VERSION_CODES.Q) {
                askPermissions()
            } else {
                goToHome()
            }
        }, AppConstants.SPLASH_DELAY)
    }

    /**
     * Metodo para abrir el home
     */
    private fun goToHome(){
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    /**
     * Manejo de permiso para almacenamiento interno
     */
    @TargetApi(Build.VERSION_CODES.M)
    fun askPermissions() {
        if (this.let {
                ContextCompat.checkSelfPermission(
                    it,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
            } != PackageManager.PERMISSION_GRANTED
        ) {
            // Sin permiso concedido
            // Mostrar un mensaje?
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
            ) {
                // Se despliega el mensaje para solicitar el permiso
                this.let {
                    AlertDialog.Builder(it)
                        .setTitle("Permiso requerido")
                        .setMessage("Permiso requerido para almacenar imagenes de internet.")
                        .setPositiveButton("Conceder") { dialog, id ->
                            ActivityCompat.requestPermissions(
                                it,
                                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                                AppConstants.MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE
                            )

                        }
                        .setNegativeButton("Denegar") { dialog, id -> dialog.cancel() }
                        .show()
                }
            } else {
                // No se requiere explicar, se pide el permiso directamente
                this.let {
                    ActivityCompat.requestPermissions(
                        it,
                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        AppConstants.MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE
                    )
                }

            }
        } else {
            // El permiso ha sido consedido
            goToHome()
        }
    }

    /**
     * Metodo que nos permite recibir la respuesta del usuario ante el mensaje de solicitud de permiso
     */
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            AppConstants.MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE -> {
                // Si la solicitud es cancelada
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // permiso concedido
                    goToHome()
                } else {
                    // permiso denegado
                    goToHome()
                }
                return
            }
            // Aqui se puede agregar la validacion para otros permisos

            else -> {
                // Por ahora nada
            }
        }
    }



}