package com.gerardo.shopper.utils

/**
 * Created by Gerardo on 7/04/2020.
 */

object AppConstants{
    val SPLASH_DELAY : Long = 3000
    val DEBUG = true
    val DEBUG_LOCAL = false

    val GET_MOVIE_LIST = 0
    val GET_MOVIE_LIST_2 = 1
    val GET_MOVIE_LIST_3 = 2

    val POSTER_URL = "https://image.tmdb.org/t/p/w185"

    val MOVIE_DATA = "Movie"

    val MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 666
    val MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 999
}