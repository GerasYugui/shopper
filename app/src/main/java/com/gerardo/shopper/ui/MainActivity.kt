package com.gerardo.shopper.ui

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gerardo.shopper.R
import com.gerardo.shopper.model.Movie
import com.gerardo.shopper.model.MovieResponse
import com.gerardo.shopper.model.ShopperError
import com.gerardo.shopper.source.local.AppDatabase
import com.gerardo.shopper.source.remote.NetHelper
import com.gerardo.shopper.ui.adapter.MovieAdapter
import com.gerardo.shopper.ui.fragment.MainFragment
import com.gerardo.shopper.ui.viewmodel.MainViewModel
import com.gerardo.shopper.utils.AppConstants
import com.gerardo.shopper.utils.AppConstants.GET_MOVIE_LIST
import com.gerardo.shopper.utils.AppConstants.GET_MOVIE_LIST_2
import com.gerardo.shopper.utils.AppConstants.GET_MOVIE_LIST_3
import com.gerardo.shopper.utils.isConnected
import com.gerardo.shopper.utils.showToast
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity(), NetHelper.OnDataResultInterface{

    private lateinit var mainViewModel: MainViewModel
    private lateinit var netHelper: NetHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, MainFragment.newInstance())
                .commitNow()
        }

        // Inicializamos el view model
        this.let { mainViewModel = ViewModelProviders.of(it).get(MainViewModel::class.java) }

        // Inicializamos nethelper para consumo de los servicios
        netHelper = NetHelper(this)

        // Se invoca el servi
        if (isConnected()){
            netHelper.getMovies("28cfef376ffc2322fef8df824918b14c", "en-US", "popularity.desc", false, false, 1)
            netHelper.getMovies("28cfef376ffc2322fef8df824918b14c", "en-US", "popularity.desc", false, false, 2)
            netHelper.getMovies("28cfef376ffc2322fef8df824918b14c", "en-US", "popularity.desc", false, false, 3)
        }else{
            getMoviesFromLocalDB()
        }




    }

    /**
     * Logica de negocio para obtener las peliculas de la base de datos local
     */
    private fun getMoviesFromLocalDB(){
        val db = AppDatabase.Companion.invoke(this)
        GlobalScope.launch {
            val data = db.movieDao().getSection(1)
            val data_2 = db.movieDao().getSection(2)
            val data_3 = db.movieDao().getSection(3)

            if (data.isNotEmpty()){
                val movies_1 : MutableList<MovieResponse> = ArrayList()
                data.forEach {
                    val movie = MovieResponse()
                    movie.id = it.id
                    movie.title = it.title
                    movie.vote_average = it.vote_average
                    movie.overview = it.overview
                    movie.page = it.page
                    movie.local = it.local
                    movie.poster_path = it.poster_path
                    movie.stringIMG = it.stringIMG
                    movies_1.add(movie)
                    if (movie.local){
                        Log.d("MoviesDB", it.stringIMG)
                    }
                }

                setMoviesInSection(movies_1, 0)
            }
            if (data_2.isNotEmpty()){
                val movies_2 : MutableList<MovieResponse> = ArrayList()
                data_2.forEach {
                    val movie = MovieResponse()
                    movie.id = it.id
                    movie.title = it.title
                    movie.vote_average = it.vote_average
                    movie.overview = it.overview
                    movie.page = it.page
                    movie.local = it.local
                    movie.poster_path = it.poster_path
                    movie.stringIMG = it.stringIMG
                    movies_2.add(movie)
                    if (movie.local){
                        Log.d("MoviesDB", it.stringIMG)
                    }
                }
                setMoviesInSection(movies_2, 1)
            }
            if (data_3.isNotEmpty()){
                val movies_3 : MutableList<MovieResponse> = ArrayList()
                data_3.forEach {
                    val movie = MovieResponse()
                    movie.id = it.id
                    movie.title = it.title
                    movie.vote_average = it.vote_average
                    movie.overview = it.overview
                    movie.page = it.page
                    movie.local = it.local
                    movie.poster_path = it.poster_path
                    movie.stringIMG = it.stringIMG
                    movies_3.add(movie)
                    if (movie.local){
                        Log.d("MoviesDB", it.stringIMG)
                    }
                }
                setMoviesInSection(movies_3, 2)
            }
        }
    }

    /**
     * Metodo para continuar con el negocio en caso de que el consumo del web service sea exitoso
     */
    override fun OnResultOk(any: Any, code: Int) {
        TODO("Not yet implemented")
    }

    /**
     * Metodo para continuar con el negocio en caso de que el consumo del web service haya tenido un error
     */
    override fun OnError(error: ShopperError, code: Int) {
        TODO("Not yet implemented")
    }

    /**
     * Metodo para continuar con el negocio en caso de que el consumo del web service sea exitoso y nos regrese un arreglo de datos
     */
    override fun OnCollectionResult(results: MutableList<Any>, code: Int) {
        val res = results as MutableList<MovieResponse>
        setMoviesInSection(results, code)
    }

    private fun setMoviesInSection(results: MutableList<MovieResponse>, code: Int){
        when (code) {
            GET_MOVIE_LIST -> {
                results.forEach {
                    Log.d("Movies", it.title)
                }

                mainViewModel.popular.postValue(results)
            }
            GET_MOVIE_LIST_2 -> {
                mainViewModel.yourSelection.postValue(results)
            }
            GET_MOVIE_LIST_3 -> {
                mainViewModel.trends.postValue(results)
            }
        }
    }

}
