package com.gerardo.shopper.ui.adapter

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Environment
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.gerardo.shopper.R
import com.gerardo.shopper.model.Movie
import com.gerardo.shopper.model.MovieResponse
import com.gerardo.shopper.utils.AppConstants.POSTER_URL
import com.gerardo.shopper.utils.decodeImage
import com.gerardo.shopper.utils.encodeImage
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import java.io.File
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

class MovieAdapter : RecyclerView.Adapter<MovieAdapter.ViewHolder>(), Filterable {

    lateinit var context: Context
    var movieList: MutableList<MovieResponse> = ArrayList()
    var movieFilterList: MutableList<MovieResponse> = ArrayList()
    var resultList: MutableList<MovieResponse> = ArrayList()
    lateinit var itemClickListener: OnItemClickListener
    lateinit var itemTitleClickListener: OnTitleItemClickListener
    var page = 0

    fun MovieAdapter(context: Context, movieList : MutableList<MovieResponse>, itemClickListener: OnItemClickListener, itemTitleItemClickListener: OnTitleItemClickListener, page: Int){
        this.context = context
        this.movieList = movieList
        this.itemClickListener = itemClickListener
        this.itemTitleClickListener = itemTitleItemClickListener
        this.page = page
        this.movieFilterList = movieList
    }

    /**
     * Interface para el correcto manejo del onclick sobre el item
     */
    interface OnItemClickListener{
        fun onItemClicked(movie: MovieResponse, moviewImageView: ImageView, page: Int)
    }

    /**
     * Interface para el correcto manejo del onclick sobre el item
     */
    interface OnTitleItemClickListener{
        fun onTitleItemClicked(movie: MovieResponse, page: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.movie_item, parent, false))
    }

    override fun getItemCount(): Int {
        return movieFilterList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = movieFilterList[position]
        holder.bind(item, context, itemClickListener, itemTitleClickListener, page)
    }

    override fun getFilter(): Filter {
        return object : Filter(){
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty() || charSearch.length < 3){
                    movieFilterList = movieList
                    resultList.clear()
                }else if (charSearch.length >= 3){
                    Log.d("MoviesFilter", charSearch.toLowerCase(Locale.ROOT))
                    for (row in movieList){
                        Log.d("MoviesFilter", row.title.toLowerCase(Locale.ROOT))
                        if (row.title.toLowerCase(Locale.ROOT).contains(charSearch.toLowerCase(Locale.ROOT))){
                            if (!resultList.contains(row))
                                resultList.add(row)
                        }
                    }
                    movieFilterList = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = movieFilterList
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                movieFilterList = results?.values as MutableList<MovieResponse>
                notifyDataSetChanged()
            }

        }
    }

    /**
     * Clase encargada de la inicialización de cada item con la información correcpondiente
     */
    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val img = view.findViewById(R.id.item_movie_img) as  ImageView
        val title = view.findViewById<TextView>(R.id.item_movie_title)

        fun bind(movie: MovieResponse, context: Context, itemClickListener: OnItemClickListener, itemTitleItemClickListener: OnTitleItemClickListener, page: Int){

            title.text = movie.title
            if (movie.local){
                //val imageByteArray: ByteArray = Base64.decode(movie.stringIMG, Base64.DEFAULT)
                Glide.with(context).load(context.decodeImage(movie.stringIMG)).into(img)
            }else{
                Glide.with(context).load(POSTER_URL + movie.poster_path).into(img)
            }

            // Listener para abrir el detalle de la pelicula
            img.setOnClickListener {
                itemClickListener.onItemClicked(movie, img, page)
            }

            // Listener para mostrar el dialogfragment
            title.setOnClickListener {
                itemTitleItemClickListener.onTitleItemClicked(movie, page)
            }
        }
    }
}