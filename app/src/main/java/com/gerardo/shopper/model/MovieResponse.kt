package com.gerardo.shopper.model

import android.graphics.Bitmap
import java.io.Serializable

/**
 * Created by Gerardo on 11/04/2020.
 * Modelo de datos que seran recibidos desde el web servide de The movie data base
 */
data class MovieResponse(
    var popularity: Double = 0.0,
    var vote_count: Int = 0,
    var video: Boolean = false,
    var poster_path: String = "",
    var id: Int = 0,
    var adult: Boolean = false,
    var backdrop_path: String = "",
    var original_language: String = "",
    var original_title: String = "",
    var title: String = "",
    var vote_average: Double = 0.0,
    var overview: String = "",
    var release_date: String = "",
    var page: Int = 0,
    var local: Boolean = false,
    var stringIMG: String = ""
): Serializable{
    constructor(id: Int, title: String, vote_average: Double, overview: String, poster_path: String, page: Int, local: Boolean, stringIMG: String) : this()
}