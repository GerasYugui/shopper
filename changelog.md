Registro de cambios entre versiones para la aplicación de partner

## [Unreleased]

## [0.0.1] - 05-03-2020
### Added
- Modelo Movie
- Recycler view horizontal
- Seccion de animación y comedia
- Splash Screen Activity
- Clase para View Dialog (Loader)
- Retrofit para consumo de Web Services
- Clase para manejo de Errores controlados en la aplicación
- Modelo de datos de "The movie data base"
- Clase para constantes globales
- Request de peliculas y despliegue en vista principal.
- Actividad con el detalle de la pelicula.
- Custom dialog fragment para mostrar rating de la pelicula.
- Se agrega Room, se crea data base, daos y entities
- Pruebas unitarias a la base de datos local
- Guardar las peliculas consultadas en la base de datos local.
- Flujo de datos para leer las peliculas locales cuando no hay acceso a internet
- Se guardan las imagenes localmente para poder parsearlas a base64
- Buscador de peliculas.

## Changed
- Metodo para guardar las imagenes en la base local y no consumir memoria del dispositivo.