package com.gerardo.shopper.model

/**
 * Creado por Gerardo Tinajero 7/04/2020
 * Clase para manejo de Errores de la aplicación controlados
 */
data class ShopperError(
    var error: Int
)